from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from flask import Flask, request
import time
import numpy as np
import os.path

app = Flask(__name__)
@app.route("/",methods = ['POST'])
def home():
    global key
    key = request.form['name']
    data = process_question(key)
    try:   
        week = data[1]
        player_info = data[0]
        real_week = get_schedule(player_info[-2],week)
        name = player_info[2].lower().capitalize()+' '+ player_info[3].lower().capitalize()
    except:
        pass
    if real_week == 0:
        end = ''
    else:
        try:
            return '\n%s got %.2f points in week %s'%(name,read_csv_data(week = real_week,info = player_info),week)
        except Exception as e:
            return 'There was an error getting data for that player. \nWe have added this to the fixme log'
            f= open('Broken_players.txt','a')
            f.write('Player Name: '+player_info[2]+'\n'+'Week: '+ str(week) +'\n'+ 'Error:' +str(e)+'\n\n\n')
    
def update_player_list():
    # Updates a list of the top 300 fantasy players, their id's and position to a text file
    x = 1
    f = open('Top_players.txt', 'w')
    errorFile = open('errorfile.txt', 'w')
    req = Request('https://www.pro-football-reference.com/years/2018/fantasy.htm',
                  headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(urlopen(req), features="html.parser")
    tablestats = soup.find("table", {'class': 'sortable stats_table'})
    f.write(str(time.asctime()) + '\n')
    table = tablestats.findAll('tr')[2:]
    del table[29::31]
    for row in table:
        if x != 301:
            name = row.findAll('td', {'data-stat': 'player'})
            pos = row.findAll('td', {'data-stat': "fantasy_pos"})
            team = row.findAll('td',{'data-stat':'team'})
            try:
                f.write(str(x) + ' ' + str(name).split(' ')[4][17:-1] + ' ' + name[0].a.string + ' '+team[-1].a.string+' ' + str(pos)[-8:-6] + '\n')
            except Exception as e:
                errorFile.write(str(x) + '####' + str(e) + '#####' + str(name) + '\n')
                pass
            x += 1
        else:
            break
    f.write('301')
    f.close
    errorFile.close
    del table


def find_player_data(num, idnum, pos):
    # Parses game data for a player from pro-football-reference.com and puts it into a csv file
    x = 0
    direct = 'C:/Users/clayf/Desktop/Python/Nfl stats'
    if pos.strip() == 'QB':
        stats = ["pass_yds", 'pass_td', 'pass_int', 'pass_sacked_yds', 'rush_yds', 'rush_td']
    else:
        stats = ['rec_yds', 'rec_td', 'rush_yds', 'rush_td']
    f = open(os.path.join(direct, str(num) + '.csv'), 'w')
    errorFile = open('errorfile_player.txt', 'w')
    player_url = 'https://www.pro-football-reference.com/players/{0}/{1}/gamelog/2018/'.format(idnum[0], idnum)
    req = Request(player_url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(urlopen(req), features="html.parser")
    tablestats = soup.find('table', {'class': "row_summable sortable stats_table"})
    for row in tablestats.findAll('tr')[2:-1]:
        x += 1
        for stat in stats:
            collect = row.find('td', {'data-stat': stat})

            try:
                f.write(
                    str(collect).strip('</td>').strip('class="right " data-stat=' + stat + '>').split('>')[-1] + ',')
            except Exception as e:
                errorFile.write(str(x) + '####' + str(e) + '#####' + str(collect))
                pass
        f.write('\n')
    f.close
    errorFile.close


def write_csv_files(startnum=1, endnum=300):
    # Writes a csv file for each player on the top 300 list with data for every week
    f = open('Top_players.txt', 'r')
    line = f.readline()
    for i in range(startnum):
        line = f.readline()
    while int(line.split()[0]) <= endnum:
        linelist = line.split()
        if linelist[-1].strip() == 'QB':
            try:
                find_player_data(linelist[0], linelist[1], linelist[-1])
            except:
                print('Error reading player data on line: {0}. Line:{1}'.format(linelist[0], line))
                pass
            print(linelist[0])
            time.sleep(4)
        line = f.readline()
    f.close

def get_schedule(team,week):
    #reads csv file of schedule
    #team = get_player_info(player)[-2].strip()
    team = team.strip()
    f = open('Schedule.csv','r')
    lines = f.readlines()
    schedule = {}
    for line in lines:
        line = line.split(',')
        if line[0]==team.upper():
            opponents = line[1:]
    for i in range(1,18):
        schedule[i] = opponents[i-1]
    if week <=0 or week>17:
        print('Invalid week number. \nPlease enter a number between 1 and 17')
        return 0
    elif week >=13:
        return week - 1
    elif week<4:
        return week
    elif schedule[week]=='BYE':
        print('Player had a bye during week %d'%(week))
        return 0
    else:
        for i in range(4,week+1):
            if schedule[i]=='BYE':
                return week-1
            else:
                return week
    
def get_player_info(name):
    f = open('Top_players.txt', 'r')
    lines = f.readlines()[1:]
    for line in lines:
        if name in line:
            data = line
            break
    return data.split()
    f.close
    
def read_csv_data(week,name = 0,info = 0):
    # Calculates fantasy points based on a week
    if name != 0:
        info = get_player_info(name)
        num = info[0]
        pos = info[-1]
    elif info !=0:
        num = info[0]
        pos = info[-1]
    direct = 'C:/Users/clayf/Desktop/Python/Nfl stats'
    f = open(os.path.join(direct, num + '.csv'), 'r')
    lines = f.readlines()
    data = [int(i) for i in lines[week - 1].split(',')[:-1]]
    # points= calculate_points(pos,[int(i) for i in data])
    f.close
    if pos.strip() == 'QB':
        # data should be list with [passing yds, passing tds, sacked yds, rushing yds, rushing tds]
        return np.dot(data, [1 / 25, 6, -1, -.1, .1, 6])
    else:
        # data should be [recieving yds, receiving tds, rushing yds, rushing tds]
        return np.dot(data, [.1, 6, .1, 6])

def process_question(key1):
    question = key1.upper()
    f=open('Top_players.txt','r')
    lines = f.readlines()[1:]
    try:
        for line in lines:
            line = line.upper()
            fname = line.split()[2]
            lname = line.split()[3]
            name = fname+' '+ lname
            if name in question:
                player_name=line.split()
                break
    except Exception as e:
        print('Could not find player name in question. \nPlease try another question\n{0}'.format(str(e)))
    weeks = {'ONE':1,'TWO':2,'THREE':3,'FOUR':4,'FIVE':5,'SIX':6,'SEVEN':7,'EIGHT':8,'NINE':9,'TEN':10,'ELEVEN':11,'TWELVE':12,'THIRTEEN':13,'FOURTEEN':14,'FIFTEEN':15,'SIXTEEN':16,'SEVENTEEN':17}
    for c in question.split():
        if weeks.get(c):
            return player_name,weeks[c]
if __name__ == '__main__':
    app.run()
    